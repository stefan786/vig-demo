// this is an example script file to demonstrate the Jira/git integration
// depending on the Jira addon, changes in code aren't added as comment
// but rather in the Activity Log of an issue

import moment from 'moment'

function precisionRound(num) {
  return isNaN(num) ? undefined : Math.round((num + Number.EPSILON) * 100) / 100; //Number.EPSILON prevents rounding errors
}

function calcTimeDiff(earlierTime, laterTime) {
  const from = moment.utc(earlierTime, 'HH:mm');
  const to = moment.utc(laterTime, 'HH:mm');
  return to.diff(from, 'hours', true);
}

function anotherFunction(paramA, paramB) {
  const mergedText = paramA + paramB;
  return mergedText;
}

function moreTestFunction(ticketID, messageCode) {
  const messageText = messageCode + "\n" + ticketID;
  return messageText;
}

function makeNumberSigned(number, customZeroPrefix, reverse) {
  if (isNaN(number)) {
    return '-';
  }
  if (number == 0) {
    const zeroPrefix = customZeroPrefix === undefined ? '+/-' : customZeroPrefix;
    return zeroPrefix + number;
  } else {
    let numberPrefix = number > 0 ? '+' : '';
    if (number > 0 && reverse == true) {
      numberPrefix = '-';
    }
    return numberPrefix + number;
  }
}

export {
  precisionRound,
  calcTimeDiff,
  makeNumberSigned,
  anotherFunction,
  moreTestFunction
}